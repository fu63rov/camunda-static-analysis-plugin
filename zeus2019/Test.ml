let request = O
let score = S request
let rating = S score
let reply = S rating
let risk = O
let process = (Sequence ((Activity (Allocate request)), (Sequence ((Activity (Source score)), (Sequence ((Activity (Write (request, risk, score))), (Sequence ((Choice ((Activity (Read (rating, request, risk))), (Activity (Allocate rating)))), (Sequence ((Activity (Sanitize (reply, rating))), Empty))))))))))
let result_request = (match analysis process request with
  | NotTainted -> "Not tainted"
  | MaybeTainted -> "Maybe tainted")
let result_score = (match analysis process score with
  | NotTainted -> "Not tainted"
  | MaybeTainted -> "Maybe tainted")
let result_rating = (match analysis process rating with
  | NotTainted -> "Not tainted"
  | MaybeTainted -> "Maybe tainted")
let result_reply = (match analysis process reply with
  | NotTainted -> "Not tainted"
  | MaybeTainted -> "Maybe tainted")

let () = Printf.printf "variable request: %s, variable score: %s, variable rating: %s, variable reply: %s, \n" request score rating reply 
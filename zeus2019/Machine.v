Require Import List Wf.

Require Import ListUtil.

Set Implicit Arguments.

Section reachable.

  Variables state activity : Set.
  Variable exec : activity -> state -> state.

  Inductive compound_activity : Set :=
  | Activity : activity -> compound_activity
  | Sequence : compound_activity -> compound_activity -> compound_activity
  | Choice : compound_activity -> compound_activity -> compound_activity
  | Loop : compound_activity -> compound_activity
  | Parallel : compound_activity -> compound_activity -> compound_activity
  | Empty : compound_activity.

  Fixpoint activities proc : list activity :=
    match proc with
    | Activity a => a::nil
    | Sequence c1 c2 => activities c1 ++ activities c2
    | Choice c1 c2 => activities c1 ++ activities c2
    | Loop c => activities c
    | Parallel c1 c2 => activities c1 ++ activities c2
    | Empty => nil
    end.

  Variable process : compound_activity.
  Definition process_as_list := activities process.

  Fixpoint contains a c {struct c} : Prop :=
    match c with
    | Activity a' => a = a'
    | Sequence c1 c2 => contains a c1 \/ contains a c2
    | Choice c1 c2 => contains a c1 \/ contains a c2
    | Loop c => contains a c
    | Parallel c1 c2 => contains a c1 \/ contains a c2
    | Empty => False
    end.

  Hint Resolve in_or_app.

  Inductive reachable_flowInsensitive : state -> state -> Prop :=
    | Rfi_Done : forall s, reachable_flowInsensitive s s
    | Rfi_Step : forall s1 s2 s3 a,
      In a process_as_list
        -> s2 = exec a s1
        -> reachable_flowInsensitive s2 s3
        -> reachable_flowInsensitive s1 s3.

  Hint Constructors reachable_flowInsensitive.

  Lemma reachable_flowInsensitive_transitive:
    forall s1 s2 s3,
    reachable_flowInsensitive s1 s2
      -> reachable_flowInsensitive s2 s3
      -> reachable_flowInsensitive s1 s3.
    induction 1; intros. auto. econstructor; eauto.
  Qed. 

  Lemma contains_implies_in_list : forall a,
    contains a process -> In a process_as_list.
    unfold process_as_list.
    induction process; intros.

    assert (a0 = a).
    congruence.
    subst.
    unfold activities.
    apply in_eq.

    destruct H.
    apply IHc1 in H; unfold activities; eauto.
    apply IHc2 in H; unfold activities; eauto.

    destruct H.
    apply IHc1 in H; unfold activities; eauto.
    apply IHc2 in H; unfold activities; eauto.

    eauto.

    destruct H.
    apply IHc1 in H; unfold activities; eauto.
    apply IHc2 in H; unfold activities; eauto.

    assert False; auto.
  Qed.

  Hint Resolve contains_implies_in_list.

  Inductive step : compound_activity -> state -> compound_activity -> state -> Prop :=
    | S_Activity : forall a s1 s2,
      contains a process
        -> s2 = exec a s1
        -> step (Activity a) s1 Empty s2
    | S_Sequence_Empty : forall c s, step (Sequence Empty c) s c s
    | S_Sequence_Left : forall c c1 c2 s1 s2,
      step c1 s1 c2 s2
        -> step (Sequence c1 c) s1 (Sequence c2 c) s2
    | S_Choice_True : forall c1 c2 s,
      step (Choice c1 c2) s c1 s
    | S_Choice_False : forall c1 c2 s,
      step (Choice c1 c2) s c2 s
    | S_Loop_True : forall c s,
      step (Loop c) s (Sequence c (Loop c)) s
    | S_Loop_False : forall c s,
      step (Loop c) s Empty s
    | S_Parallel_Left : forall c1 c2 c s1 s2,
      step c1 s1 c2 s2
        -> step (Parallel c1 c) s1 (Parallel c2 c) s2
    | S_Parallel_Right : forall c1 c2 c s1 s2,
      step c1 s1 c2 s2
        -> step (Parallel c c1) s1 (Parallel c c2) s2
    | S_Parallel_Empty : forall s,
      step (Parallel Empty Empty) s Empty s.

  Hint Constructors step.

  Inductive reachable : compound_activity -> state -> compound_activity -> state -> Prop :=
    | R_Done : forall c s, reachable c s c s
    | R_Step : forall c1 c2 c3 s1 s2 s3,
      step c1 s1 c2 s2
        -> reachable c2 s2 c3 s3
        -> reachable c1 s1 c3 s3.

  Theorem flowInsensitive_is_conservative :
    forall c1 s1 c3 s3, reachable c1 s1 c3 s3
      -> reachable_flowInsensitive s1 s3.
    induction 1; eauto.
    apply reachable_flowInsensitive_transitive with (s2:=s2); eauto.
    clear H0. clear IHreachable.
    induction H; eauto.
  Qed.

  Section approx.
    Variable approx : state -> state -> Prop.
    Variable approxStrict : state -> state -> Prop.

    Hypothesis approx_refl : forall s,
      approx s s.

    Hypothesis approx_trans : forall s1 s2 s3,
      approx s1 s2
      -> approx s2 s3
      -> approx s1 s3.

    Hypothesis increasing : forall s a,
      approx s (exec a s).

    Hypothesis monotonic : forall s1 s2 a,
      approx s1 s2
      -> approx (exec a s1) (exec a s2).

    Section fixed_point.
      Variable fp : state.

      Hypothesis fp_ok : forall a,
	In a process_as_list
	  -> approx (exec a fp) fp.
      
      Theorem fixed_point_ok : forall s,
	approx s fp
	-> forall s', reachable_flowInsensitive s s'
	  -> approx s' fp.
        induction 2; subst; intuition eauto.
      Qed.
    End fixed_point.

    Hypothesis approxStrict_approx : forall s1 s2,
      approx s2 s1
      -> ~approxStrict s1 s2
      -> approx s1 s2.

    Hypothesis approxStrict_trans : forall s1 s2 s3,
      approx s2 s1
      -> approxStrict s2 s3
      -> approxStrict s1 s3.

    Variable approxStrict_dec : forall x y, {approxStrict x y} + {~approxStrict x y}.

    Hypothesis approxStrict_wf : well_founded approxStrict.

    Variable init : state.

    Hypothesis init_low : forall s, approx init s.

    Lemma iterate_approx : forall proc s,
      approx s (fold_left (fun s a => exec a s) proc s).
      induction proc; simpl; intuition eauto.
    Qed.

    Hint Resolve iterate_approx.

    Lemma iterate_pick' : forall a proc s s',
      In a proc
      -> approx s s'
      -> approxStrict (exec a s) s
      -> approxStrict
      (fold_left (fun st a => exec a st)
        proc s') s.
      induction proc; simpl; intuition; subst.
      
      apply approxStrict_trans with (exec a s); trivial.
      apply approx_trans with (exec a s'); eauto.

      apply IHproc; auto.
      apply approx_trans with (exec a0 s); eauto.
    Qed.

    Lemma iterate_pick_cp : forall a s,
      In a process_as_list
	-> approxStrict (exec a s) s
	-> approxStrict
	(fold_left (fun st a => exec a st)
          process_as_list s) s.
      intros.
      eapply iterate_pick'; eauto.
    Qed.

    Lemma iterate_pick : forall a s,
      In a process_as_list
	-> ~approxStrict
	(fold_left (fun st a => exec a st)
          process_as_list s) s
	-> ~approxStrict (exec a s) s.
      generalize iterate_pick_cp; firstorder.
    Qed.

    Definition iterate : {fp : state
      | approx init fp
	/\ forall a,
	  In a process_as_list
	    -> approx (exec a fp) fp}.
      pose (iter := Fix approxStrict_wf
	(fun _ => state)
	(fun s self =>
	  let s' := fold_left (fun st a => exec a st) process_as_list s in
	    match approxStrict_dec s' s with
	      | left pf => self s' pf
	      | _ => s
	    end)).

      exists (iter init).
      intros.

      assert (Happrox : forall x, approx x (iter x)).
      intro x; pattern x.
      eapply well_founded_ind; intuition.
      eauto.
      unfold iter; rewrite Fix_eq.
      match goal with
	| [ |- context[approxStrict_dec ?X ?Y] ] => destruct (approxStrict_dec X Y)
      end.
      fold iter.
      apply approx_trans with (fold_left (fun (st : state) (a0 : activity) => exec a0 st)
        process_as_list x0); auto.
      auto.
      intuition.
      match goal with
	| [ |- context[approxStrict_dec ?X ?Y] ] => destruct (approxStrict_dec X Y)
      end; trivial.

      assert (HapproxStrict : forall x, ~approxStrict
	(fold_left (fun st a => exec a st) process_as_list (iter x)) (iter x)).
      intro x; pattern x.
      eapply well_founded_ind; intuition.
      eauto.
      unfold iter in H0; rewrite Fix_eq in H0.
      match goal with
	| [ H : context[approxStrict_dec ?X ?Y] |- _ ] => destruct (approxStrict_dec X Y)
      end; auto.
      eauto.
      intuition.
      match goal with
	| [ |- context[approxStrict_dec ?X ?Y] ] => destruct (approxStrict_dec X Y)
      end; trivial.

      intuition.
      apply approxStrict_approx.
      auto.
      apply iterate_pick; eauto.
    Qed.

    Definition fixed_point : {fp : state
      | forall s, reachable_flowInsensitive init s
	-> approx s fp}.
      destruct iterate as [fp [Hinit Hfp]].
      exists fp.

      clear init_low.
      induction 1; try subst; intuition eauto.
    Qed.

  End approx.
End reachable.
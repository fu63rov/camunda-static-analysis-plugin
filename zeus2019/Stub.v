Require Import Andersen.
Require Import Machine.
Require Import Pointer.
Require Import List.

Fixpoint list_to_process list : compound_activity activity :=
    match list with
    | nil => Empty activity
    | a :: m => Parallel (Loop (Activity a)) (list_to_process m) 
    end.

Variable process : list activity.
Definition stub := analysis (list_to_process process).

Recursive Extraction stub.
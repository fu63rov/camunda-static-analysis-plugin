let risk = O
let score = S risk
let reply = S score
let process = Cons ((Copy (reply, risk)), (Cons ((Copy (risk, score)), (Cons ((Source score), (Nil))))))
let stub = 
 analysis (list_to_process process)

let result_reply = (match stub reply with
  | NotTainted -> "Not tainted"
  | MaybeTainted -> "Maybe tainted")

let () = Printf.printf "{\"reply\":\"%s\"}\n" result_reply 
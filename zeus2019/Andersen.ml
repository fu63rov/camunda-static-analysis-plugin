type bool =
| True
| False

type nat =
| O
| S of nat

type ('a, 'b) prod =
| Pair of 'a * 'b

type 'a list =
| Nil
| Cons of 'a * 'a list

(** val app : 'a1 list -> 'a1 list -> 'a1 list **)

let rec app l m =
  match l with
  | Nil -> m
  | Cons (a, l1) -> Cons (a, (app l1 m))

type 'a sig0 =
  'a
  (* singleton inductive, whose constructor was exist *)

type sumbool =
| Left
| Right

(** val eq_nat_dec : nat -> nat -> sumbool **)

let rec eq_nat_dec n m =
  match n with
  | O ->
    (match m with
     | O -> Left
     | S m0 -> Right)
  | S n0 ->
    (match m with
     | O -> Right
     | S m0 -> eq_nat_dec n0 m0)

(** val map : ('a1 -> 'a2) -> 'a1 list -> 'a2 list **)

let rec map f = function
| Nil -> Nil
| Cons (a, t) -> Cons ((f a), (map f t))

(** val fold_left : ('a1 -> 'a2 -> 'a1) -> 'a2 list -> 'a1 -> 'a1 **)

let rec fold_left f l a0 =
  match l with
  | Nil -> a0
  | Cons (b, t) -> fold_left f t (f a0 b)

(** val in_eq_dec : ('a1 -> 'a1 -> sumbool) -> 'a1 -> 'a1 list -> sumbool **)

let rec in_eq_dec eq_dec x = function
| Nil -> Right
| Cons (y, l) ->
  (match in_eq_dec eq_dec x l with
   | Left -> Left
   | Right -> eq_dec y x)

(** val incl_eq_dec :
    ('a1 -> 'a1 -> sumbool) -> 'a1 list -> 'a1 list -> sumbool **)

let rec incl_eq_dec eq_dec ls1 ls2 =
  match ls1 with
  | Nil -> Left
  | Cons (y, l) ->
    let s = in_eq_dec eq_dec y ls2 in
    (match s with
     | Left -> incl_eq_dec eq_dec l ls2
     | Right -> Right)

(** val allS_dec : ('a1 -> sumbool) -> 'a1 list -> sumbool **)

let rec allS_dec f_dec = function
| Nil -> Left
| Cons (y, l) ->
  (match allS_dec f_dec l with
   | Left -> f_dec y
   | Right -> Right)

(** val allS_dec_some : ('a1 -> sumbool) -> 'a1 list -> sumbool **)

let allS_dec_some f_dec = function
| Nil -> Left
| Cons (a, ls0) -> allS_dec f_dec (Cons (a, ls0))

type 'activity compound_activity =
| Activity of 'activity
| Sequence of 'activity compound_activity * 'activity compound_activity
| Choice of 'activity compound_activity * 'activity compound_activity
| Loop of 'activity compound_activity
| Parallel of 'activity compound_activity * 'activity compound_activity
| Empty

(** val activities : 'a1 compound_activity -> 'a1 list **)

let rec activities = function
| Activity a -> Cons (a, Nil)
| Sequence (c1, c2) -> app (activities c1) (activities c2)
| Choice (c1, c2) -> app (activities c1) (activities c2)
| Loop c -> activities c
| Parallel (c1, c2) -> app (activities c1) (activities c2)
| Empty -> Nil

(** val process_as_list : 'a1 compound_activity -> 'a1 list **)

let process_as_list process0 =
  activities process0

(** val iterate :
    ('a2 -> 'a1 -> 'a1) -> 'a2 compound_activity -> ('a1 -> 'a1 -> sumbool)
    -> 'a1 -> 'a1 **)

let rec iterate exec process0 approxStrict_dec0 init0 =
  let s' = fold_left (fun st a -> exec a st) (process_as_list process0) init0
  in
  (match approxStrict_dec0 s' init0 with
   | Left -> iterate exec process0 approxStrict_dec0 s'
   | Right -> init0)

(** val fixed_point :
    ('a2 -> 'a1 -> 'a1) -> 'a2 compound_activity -> ('a1 -> 'a1 -> sumbool)
    -> 'a1 -> 'a1 **)

let fixed_point exec process0 approxStrict_dec0 init0 =
  iterate exec process0 approxStrict_dec0 init0

module type EQ = 
 sig 
  type dom 
  
  val dom_eq_dec : dom -> dom -> sumbool
 end

module type MAP = 
 sig 
  type dom 
  
  type 'x map 
  
  val init : 'a1 -> 'a1 map
  
  val sel : 'a1 map -> dom -> 'a1
  
  val upd : 'a1 map -> dom -> 'a1 -> 'a1 map
 end

module FuncMap = 
 functor (Param:EQ) ->
 struct 
  type 'ran map = Param.dom -> 'ran
  
  (** val init : 'a1 -> 'a1 map **)
  
  let init r x =
    r
  
  (** val sel : 'a1 map -> Param.dom -> 'a1 **)
  
  let sel m d =
    m d
  
  (** val upd : 'a1 map -> Param.dom -> 'a1 -> 'a1 map **)
  
  let upd m d r d' =
    match Param.dom_eq_dec d d' with
    | Left -> r
    | Right -> m d'
  
  type dom = Param.dom
 end

module type SET = 
 sig 
  type dom 
  
  type set 
  
  val empty : set
  
  val coq_In : dom -> set -> bool
  
  val add : set -> dom -> set
  
  val remove : set -> dom -> set
  
  val union : set -> set -> set
  
  val intersect : set -> set -> set
  
  val incl : set -> set -> bool
  
  val elems : set -> dom list
 end

module ListSet = 
 functor (Param:EQ) ->
 struct 
  type set = Param.dom list
  
  (** val empty : set **)
  
  let empty =
    Nil
  
  (** val coq_In : Param.dom -> set -> bool **)
  
  let coq_In v s =
    match in_eq_dec Param.dom_eq_dec v s with
    | Left -> True
    | Right -> False
  
  (** val add : set -> Param.dom -> set **)
  
  let add s v =
    match in_eq_dec Param.dom_eq_dec v s with
    | Left -> s
    | Right -> Cons (v, s)
  
  (** val remove : set -> Param.dom -> set **)
  
  let rec remove s v =
    match s with
    | Nil -> Nil
    | Cons (v', s') ->
      (match Param.dom_eq_dec v v' with
       | Left -> s'
       | Right -> Cons (v', (remove s' v)))
  
  (** val union : set -> set -> set **)
  
  let rec union s1 s2 =
    match s1 with
    | Nil -> s2
    | Cons (v, s1') -> add (union s1' s2) v
  
  (** val intersect : set -> set -> set **)
  
  let rec intersect s1 s2 =
    match s1 with
    | Nil -> Nil
    | Cons (v, s1') ->
      (match in_eq_dec Param.dom_eq_dec v s2 with
       | Left -> Cons (v, (intersect s1' s2))
       | Right -> intersect s1' s2)
  
  (** val incl : set -> set -> bool **)
  
  let incl s1 s2 =
    match incl_eq_dec Param.dom_eq_dec s1 s2 with
    | Left -> True
    | Right -> False
  
  (** val elems : set -> Param.dom list **)
  
  let elems s =
    s
  
  type dom = Param.dom
 end

type field = nat

type var = nat

type activity =
| Allocate of var
| Source of var
| Copy of var * var
| Sanitize of var * var
| Read of var * var * field
| Write of var * field * var

module NatEq = 
 struct 
  type dom = nat
  
  (** val dom_eq_dec : nat -> nat -> sumbool **)
  
  let dom_eq_dec =
    eq_nat_dec
 end

module NatMap = FuncMap(NatEq)

module VarMap = NatMap

(** val eq_natboolpair_dec :
    (nat, bool) prod -> (nat, bool) prod -> sumbool **)

let eq_natboolpair_dec x y =
  let Pair (x0, x1) = x in
  let Pair (n, b0) = y in
  (match let rec f n0 n1 =
           match n0 with
           | O ->
             (match n1 with
              | O -> Left
              | S n2 -> Right)
           | S n2 ->
             (match n1 with
              | O -> Right
              | S n3 -> f n2 n3)
         in f x0 n with
   | Left ->
     (match x1 with
      | True ->
        (match b0 with
         | True -> Left
         | False -> Right)
      | False ->
        (match b0 with
         | True -> Right
         | False -> Left))
   | Right -> Right)

module NatBoolPairEq = 
 struct 
  type dom = (nat, bool) prod
  
  (** val dom_eq_dec : (nat, bool) prod -> (nat, bool) prod -> sumbool **)
  
  let dom_eq_dec =
    eq_natboolpair_dec
 end

type object0 = (nat, bool) prod

(** val cell : object0 -> nat **)

let cell = function
| Pair (x, y) -> x

type allocation_site = nat

module AllocMap = NatMap

module AllocSet = ListSet(NatBoolPairEq)

type mustNotBeTainted_answer =
| MaybeTainted
| NotTainted

type mustNotBeTainted_procedure =
  activity compound_activity -> var -> mustNotBeTainted_answer

type abstract_activity =
| AbsAllocate of var * allocation_site
| AbsSource of var * allocation_site
| AbsCopy of var * var
| AbsSanitize of var * var
| AbsRead of var * var * field
| AbsWrite of var * field * var

type abstract_state = { avars : AllocSet.set VarMap.map;
                        aheap : AllocSet.set AllocMap.map }

(** val avars : abstract_state -> AllocSet.set VarMap.map **)

let avars x = x.avars

(** val aheap : abstract_state -> AllocSet.set AllocMap.map **)

let aheap x = x.aheap

(** val abstract_exec :
    abstract_activity -> abstract_state -> abstract_state **)

let abstract_exec i s =
  match i with
  | AbsAllocate (dst, site) ->
    { avars =
      (VarMap.upd s.avars dst
        (AllocSet.add (VarMap.sel s.avars dst) (Pair (site, False))));
      aheap = s.aheap }
  | AbsSource (dst, site) ->
    { avars =
      (VarMap.upd s.avars dst
        (AllocSet.add (VarMap.sel s.avars dst) (Pair (site, True)))); aheap =
      s.aheap }
  | AbsCopy (dst, src) ->
    { avars =
      (VarMap.upd s.avars dst
        (AllocSet.union (VarMap.sel s.avars dst) (VarMap.sel s.avars src)));
      aheap = s.aheap }
  | AbsSanitize (dst, src) ->
    { avars =
      (VarMap.upd s.avars dst
        (fold_left (fun known site ->
          AllocSet.union known
            (AllocSet.add AllocSet.empty (Pair ((cell site), False))))
          (AllocSet.elems (VarMap.sel s.avars src)) (VarMap.sel s.avars dst)));
      aheap = s.aheap }
  | AbsRead (dst, src, fl) ->
    { avars =
      (VarMap.upd s.avars dst
        (fold_left (fun known site ->
          AllocSet.union known (AllocMap.sel s.aheap (cell site)))
          (AllocSet.elems (VarMap.sel s.avars src)) (VarMap.sel s.avars dst)));
      aheap = s.aheap }
  | AbsWrite (dst, fl, src) ->
    { avars = s.avars; aheap =
      (fold_left (fun known site ->
        AllocMap.upd known (cell site)
          (AllocSet.union (AllocMap.sel s.aheap (cell site))
            (VarMap.sel s.avars src)))
        (AllocSet.elems (VarMap.sel s.avars dst)) s.aheap) }

(** val abstractProcess' :
    allocation_site -> activity compound_activity -> abstract_activity
    compound_activity **)

let rec abstractProcess' site = function
| Activity a ->
  (match a with
   | Allocate dst -> Activity (AbsAllocate (dst, site))
   | Source dst -> Activity (AbsSource (dst, site))
   | Copy (dst, src) -> Activity (AbsCopy (dst, src))
   | Sanitize (dst, src) -> Activity (AbsSanitize (dst, src))
   | Read (dst, src, fl) -> Activity (AbsRead (dst, src, fl))
   | Write (dst, fl, src) -> Activity (AbsWrite (dst, fl, src)))
| Sequence (c1, c2) ->
  Sequence ((abstractProcess' (S site) c1), (abstractProcess' (S site) c2))
| Choice (c1, c2) ->
  Choice ((abstractProcess' (S site) c1), (abstractProcess' (S site) c2))
| Loop c -> Loop (abstractProcess' (S site) c)
| Parallel (c1, c2) ->
  Parallel ((abstractProcess' (S site) c1), (abstractProcess' (S site) c2))
| Empty -> Empty

(** val abstractProcess :
    activity compound_activity -> abstract_activity compound_activity **)

let abstractProcess process0 =
  abstractProcess' (S O) process0

(** val abstract_initState : abstract_state **)

let abstract_initState =
  { avars = (VarMap.init AllocSet.empty); aheap =
    (AllocMap.init AllocSet.empty) }

(** val ins_sites : abstract_activity -> AllocSet.set **)

let ins_sites = function
| AbsAllocate (v, site) ->
  AllocSet.add (AllocSet.add AllocSet.empty (Pair (site, False))) (Pair
    (site, True))
| AbsSource (v, site) ->
  AllocSet.add (AllocSet.add AllocSet.empty (Pair (site, True))) (Pair (site,
    False))
| _ -> AllocSet.empty

module VarSet = ListSet(NatEq)

(** val ins_vars : abstract_activity -> VarSet.set **)

let ins_vars = function
| AbsAllocate (v, a0) -> VarSet.add VarSet.empty v
| AbsSource (v, a0) -> VarSet.add VarSet.empty v
| AbsCopy (v1, v2) -> VarSet.add (VarSet.add VarSet.empty v1) v2
| AbsSanitize (v1, v2) -> VarSet.add (VarSet.add VarSet.empty v1) v2
| AbsRead (v1, v2, fl) -> VarSet.add (VarSet.add VarSet.empty v1) v2
| AbsWrite (v1, fl, v2) -> VarSet.add (VarSet.add VarSet.empty v1) v2

(** val pSites : abstract_activity list -> AllocSet.set -> AllocSet.set **)

let pSites process_as_list1 =
  fold_left AllocSet.union (map ins_sites process_as_list1)

(** val pVars : abstract_activity list -> VarSet.set -> VarSet.set **)

let pVars process_as_list1 =
  fold_left VarSet.union (map ins_vars process_as_list1)

(** val process_as_list0 :
    abstract_activity compound_activity -> abstract_activity list **)

let process_as_list0 process0 =
  activities process0

(** val procSites : abstract_activity compound_activity -> AllocSet.set **)

let procSites process0 =
  pSites (process_as_list0 process0) AllocSet.empty

(** val procVars : abstract_activity compound_activity -> VarSet.set **)

let procVars process0 =
  pVars (process_as_list0 process0) VarSet.empty

type astate = abstract_state

type aactivity = abstract_activity

(** val aexec :
    abstract_activity compound_activity -> aactivity -> astate -> astate **)

let aexec process0 a abs =
  abstract_exec a abs

(** val ainitState : abstract_activity compound_activity -> astate **)

let ainitState process0 =
  abstract_initState

(** val approx_dec :
    abstract_activity compound_activity -> astate -> astate -> sumbool **)

let approx_dec process0 abs1 abs2 =
  let checkVar = fun v ->
    let b = AllocSet.incl (VarMap.sel abs1.avars v) (VarMap.sel abs2.avars v)
    in
    (match b with
     | True -> Left
     | False -> Right)
  in
  let checkHeap = fun v ->
    let b =
      AllocSet.incl (AllocMap.sel abs1.aheap (cell v))
        (AllocMap.sel abs2.aheap (cell v))
    in
    (match b with
     | True -> Left
     | False -> Right)
  in
  (match allS_dec checkVar (VarSet.elems (procVars process0)) with
   | Left -> allS_dec checkHeap (AllocSet.elems (procSites process0))
   | Right -> Right)

(** val approxStrict_dec :
    abstract_activity compound_activity -> astate -> astate -> sumbool **)

let approxStrict_dec process0 abs1 abs2 =
  let checkVar = fun v ->
    let b = AllocSet.incl (VarMap.sel abs1.avars v) (VarMap.sel abs2.avars v)
    in
    (match b with
     | True -> Left
     | False -> Right)
  in
  let checkHeap = fun v ->
    let b =
      AllocSet.incl (AllocMap.sel abs1.aheap (cell v))
        (AllocMap.sel abs2.aheap (cell v))
    in
    (match b with
     | True -> Left
     | False -> Right)
  in
  (match approx_dec process0 abs2 abs1 with
   | Left ->
     (match allS_dec_some checkVar (VarSet.elems (procVars process0)) with
      | Left ->
        (match allS_dec_some checkHeap (AllocSet.elems (procSites process0)) with
         | Left -> Right
         | Right -> Left)
      | Right -> Left)
   | Right -> Right)

(** val ainstr_widen_sequence_left :
    abstract_activity compound_activity -> abstract_activity
    compound_activity -> abstract_activity -> abstract_activity **)

let ainstr_widen_sequence_left c1 c2 h =
  h

(** val ainstr_widen_sequence_right :
    abstract_activity compound_activity -> abstract_activity
    compound_activity -> abstract_activity -> abstract_activity **)

let ainstr_widen_sequence_right c1 c2 h =
  h

(** val ainstr_widen_choice_left :
    abstract_activity compound_activity -> abstract_activity
    compound_activity -> abstract_activity -> abstract_activity **)

let ainstr_widen_choice_left c1 c2 h =
  h

(** val ainstr_widen_choice_right :
    abstract_activity compound_activity -> abstract_activity
    compound_activity -> abstract_activity -> abstract_activity **)

let ainstr_widen_choice_right c1 c2 h =
  h

(** val ainstr_widen_parallel_left :
    abstract_activity compound_activity -> abstract_activity
    compound_activity -> abstract_activity -> abstract_activity **)

let ainstr_widen_parallel_left c1 c2 h =
  h

(** val ainstr_widen_parallel_right :
    abstract_activity compound_activity -> abstract_activity
    compound_activity -> abstract_activity -> abstract_activity **)

let ainstr_widen_parallel_right c1 c2 h =
  h

(** val ainstr_widen_loop :
    abstract_activity compound_activity -> abstract_activity ->
    abstract_activity **)

let ainstr_widen_loop c h =
  h

(** val traverse :
    ('a1 -> 'a2) -> 'a1 compound_activity -> 'a2 compound_activity **)

let rec traverse f = function
| Activity a -> Activity (f a)
| Sequence (c1, c2) -> Sequence ((traverse f c1), (traverse f c2))
| Choice (c1, c2) -> Choice ((traverse f c1), (traverse f c2))
| Loop c -> Loop (traverse f c)
| Parallel (c1, c2) -> Parallel ((traverse f c1), (traverse f c2))
| Empty -> Empty

(** val aprocess' :
    abstract_activity compound_activity -> abstract_activity
    compound_activity **)

let rec aprocess' = function
| Activity a -> Activity a
| Sequence (c1, c2) ->
  Sequence ((traverse (ainstr_widen_sequence_left c1 c2) (aprocess' c1)),
    (traverse (ainstr_widen_sequence_right c1 c2) (aprocess' c2)))
| Choice (c1, c2) ->
  Choice ((traverse (ainstr_widen_choice_left c1 c2) (aprocess' c1)),
    (traverse (ainstr_widen_choice_right c1 c2) (aprocess' c2)))
| Loop c -> Loop (traverse (ainstr_widen_loop c) (aprocess' c))
| Parallel (c1, c2) ->
  Parallel ((traverse (ainstr_widen_parallel_left c1 c2) (aprocess' c1)),
    (traverse (ainstr_widen_parallel_right c1 c2) (aprocess' c2)))
| Empty -> Empty

(** val aprocess :
    abstract_activity compound_activity -> abstract_activity
    compound_activity **)

let aprocess process0 =
  aprocess' process0

(** val fixed_point' : abstract_activity compound_activity -> astate **)

let fixed_point' process0 =
  fixed_point (aexec process0) (aprocess process0) (fun x y ->
    approxStrict_dec process0 x y) (ainitState process0)

(** val fixed_point0 :
    abstract_activity compound_activity -> abstract_state **)

let fixed_point0 process0 =
  fixed_point' process0

(** val tainted : (nat, bool) prod -> AllocSet.set **)

let tainted a = match a with
| Pair (y, y0) ->
  (match y0 with
   | True -> AllocSet.add AllocSet.empty a
   | False -> AllocSet.empty)

(** val tSites : (nat, bool) prod list -> AllocSet.set **)

let tSites s =
  fold_left AllocSet.union (map tainted s) AllocSet.empty

(** val analysis : mustNotBeTainted_procedure **)

let analysis process0 v =
  let s = fixed_point0 (abstractProcess process0) in
  (match AllocSet.incl (tSites (AllocSet.elems (VarMap.sel s.avars v)))
           AllocSet.empty with
   | True -> NotTainted
   | False -> MaybeTainted)

(** val list_to_process : activity list -> activity compound_activity **)

let rec list_to_process = function
| Nil -> Empty
| Cons (a, m) -> Parallel ((Loop (Activity a)), (list_to_process m))

let risk = O
let score = S risk
let reply = S score
let process = Cons ((Copy (reply, risk)), (Cons ((Copy (risk, score)), (Cons ((Source score), (Nil))))))
let stub = 
 analysis (list_to_process process)

let result_reply = (match stub reply with
  | NotTainted -> "Not tainted"
  | MaybeTainted -> "Maybe tainted")

let () = Printf.printf "{\"reply\":\"%s\"}\n" result_reply 
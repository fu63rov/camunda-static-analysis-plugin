# Coq sources for *Certified Information Flow Analysis of Business Processes*

[![build status](https://gitlab.com/t.heinze/edoc2018/badges/master/build.svg)](https://gitlab.com/t.heinze/edoc2018/commits/master)

To run the analysis for the paper's example (assuming Ubuntu Linux):
```
$ apt-get update
$ apt-get install opam -y
$ opam init -n --comp=4.01.0 -j 2
$ opam repo add coq-released http://coq.inria.fr/opam/released
$ opam install coq.8.4pl4 && opam pin add coq 8.4pl4
$ eval `opam config env`
$ make Andersen.ml
$ cat Process.ml >> Andersen.ml
$ ocaml Andersen.ml
```

Coq sources are based upon [prior work by Adam Chlipala](http://adam.chlipala.net/itp/coq/src/).

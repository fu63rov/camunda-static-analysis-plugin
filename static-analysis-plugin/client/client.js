var registerBpmnJSPlugin = require('camunda-modeler-plugin-helpers').registerBpmnJSPlugin;
var analysisPlugin = require('./AnalysisPlugin');
var propertiesPlugin = require('./PropertiesPlugin');
var registerBpmnJSModdleExtension = require('camunda-modeler-plugin-helpers').registerBpmnJSModdleExtension;
var module = require('./analysis-moddle');


registerBpmnJSPlugin(analysisPlugin);
registerBpmnJSPlugin(propertiesPlugin);
registerBpmnJSModdleExtension(module);

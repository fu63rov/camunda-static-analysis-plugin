'use strict';

var $ = require('jquery');

var _console_event_dbg = "font-size:8pt; color:blue;";

const { ipcRenderer } = window.require('electron')
var assign = Object.assign;


function drawWarning(target, varName) {
  var svg = $(target).closest('svg');
  var c = $(document.createElementNS('http://www.w3.org/2000/svg', 'polygon'));
  c.attr("points", "0,-10 10,10 -10,10");
  c.attr("stroke", "black");
  c.attr('class', 'analysis-result-marker');
  var e = $(document.createElementNS('http://www.w3.org/2000/svg', 'text'));
  e.attr('class', 'analysis-result-marker');
  e.attr("x", 0);
  e.attr("y", 2);
  e.text("!");
  e.attr("text-anchor", "middle");
  e.attr("alignment-baseline", "middle");
  e.attr("fill", "black");
  e.attr("style", "font:bold 15px sans-serif");
  c.attr('fill', 'yellow');
  var g = $(document.createElementNS('http://www.w3.org/2000/svg', 'g'));
  var tt = $(document.createElementNS('http://www.w3.org/2000/svg', 'rect'));
  g.attr("visibility", "hidden");
  g.attr("x", 0);
  g.attr("y", 2);

  var text = $(document.createElementNS('http://www.w3.org/2000/svg', 'text'));
  tt.attr("fill", "white");
  tt.attr("stroke", "black");
  text.text("Flow from sensitive source to untrusted sink '" + varName + "'");
  text.attr("fill", "black");
  text.attr("style", "font:bold 15px sans-serif");
  text.attr("text-anchor", "left");
  text.attr("alignment-baseline", "before-edge");
  text.attr("x", 1);
  text.attr("y", 0);
  tt.attr("y", 0);
  tt.attr("x", 1);

  function absCoords(elem) {
    var offset = svg[0].getBoundingClientRect();
    var matrix = elem[0].getScreenCTM();
    var x = elem[0].getBBox().x, y = elem[0].getBBox().y;
    return {
      x: (matrix.a * x) + (matrix.c * y) + matrix.e - offset.left,
      y: (matrix.b * x) + (matrix.d * y) + matrix.f - offset.top
    };
  }

  g.append(tt);
  g.append(text);
  function showTooltip() {
    g.attr("visibility", "visible");
    tt.attr("height", text[0].getBBox().height);
    tt.attr("width", text[0].getBBox().width);
    var xy = absCoords(target);
    g.attr('transform', 'translate(' + (xy.x + 10) + ',' + (xy.y - 10) + ')');
    svg.append(g);
  }
  function hideTooltip() {
    g.attr("visibility", "hidden");
  }
  e.on("mouseenter", () => showTooltip());
  c.on("mouseenter", () => showTooltip());
  e.on("mouseout", () => hideTooltip());
  c.on("mouseout", () => hideTooltip());

  target.append(c);
  target.append(e);
}



function showAnalysisResult(varName, elementIds, analysisResult) {
  $('.analysis-result-marker').remove()
  $.each(elementIds, (i, tid) => {
    var target = $("[data-element-id='" + tid + "']");

    if (analysisResult == 'Maybe tainted') {
      drawWarning(target, varName);
    }
  });
}



function showAnalysisInProgressSpinner() {
  $('#spin-loader').show();
}

function hideAnalysisInProgressSpinner() {
  $('#spin-loader').hide();
}



var _bpmnjs = undefined;

function sendDiagramForAnalysis() {
  _bpmnjs.saveXML({}, (error, result) => {
    if (error) {
      console.log("%c saveXML Error  <" + result + ">: " + error, "color:red; font-size:6pt;");
      alert('bpmnjs.saveXML failed; see log.')
      return;
    }
    console.log("%c sending diagram for analysis.", "color:teal; font-size:6pt;");
    hideAnalysisInProgressSpinner();
    showAnalysisInProgressSpinner();
    ipcRenderer.send('senddiagram', result);
  });
}

ipcRenderer.on('getdiagram', (event, arg) => {
  console.log("%c getDiagram", _console_event_dbg);
  sendDiagramForAnalysis();
});


ipcRenderer.on('analysisResults', (event, arg) => {
  hideAnalysisInProgressSpinner();
  console.log("%c analysisResults", _console_event_dbg);
  console.log("%c RESULTS: " + arg, 'font-size:6pt; color:teal;');
  $.each(arg.analysis_result,
    (k, v) => {
      showAnalysisResult(k, arg.variables[k], v);
    });
});



function StaticAnalysisPlugin(eventBus, bpmnFactory, bpmnjs,
  canvas) {
  _bpmnjs = bpmnjs;

  eventBus.on("elements.changed", () => { sendDiagramForAnalysis(); });
  eventBus.on("shape.added", () => { sendDiagramForAnalysis(); });
  
  var div1 = $(document.createElement('div'));
  div1.attr('id', 'spin-loader');
  var t = $(document.createElement('div'));
  t.attr('class', 'loader-text');
  t.text("Analysis running");
  div1.append(t);
  var div = $(document.createElement('div'));
  div.attr('class', 'custom-loader');
  div1.append(div);
  div1.hide();
  canvas.getContainer().appendChild(div1[0]);

}

StaticAnalysisPlugin.$inject = ['eventBus', 'bpmnFactory', 'bpmnjs', 'canvas'];

module.exports = {
  __init__: ['staticAnalysisPlugin'],
  staticAnalysisPlugin: ['type', StaticAnalysisPlugin]
};


//require('bpmn-js-properties-panel/lib');
var entryFactory = require('bpmn-js-properties-panel/lib/factory/EntryFactory');
var PropertiesActivator = require('bpmn-js-properties-panel/lib/PropertiesActivator');
var CamundaPropertiesProvider = require('bpmn-js-properties-panel/lib/provider/camunda/CamundaPropertiesProvider');
var is = require('bpmn-js/lib/util/ModelUtil').is;


// https://forum.camunda.org/t/properties-panel-extension-extend-gettabs/6416
// https://github.com/bpmn-io/bpmn-js-examples/tree/master/properties-panel-extension


function SharedPropertiesProvider(eventBus, bpmnFactory, elementRegistry, elementTemplates, translate) {
    //PropertiesActivator.call(this, eventBus);
    var camunda = new CamundaPropertiesProvider(eventBus, bpmnFactory, elementRegistry, elementTemplates, translate);

    function analysisProps(group, element) {

        if (is(element, 'bpmn:ReceiveTask') || is(element, 'bmpn:scriptTask')) {
            group.entries.push(entryFactory.checkbox({
                id: 'isSensitive',
                description: 'Is a sensitive source',
                label: 'Sensitive?',
                modelProperty: 'is_sensitive'
            }));
        }
        if (is(element, 'bpmn:ScriptTask')) {
            group.entries.push(entryFactory.checkbox({
                id: 'isSanitize',
                description: 'Performs sanitization of sensitive data',
                label: 'Sanitizes?',
                modelProperty: 'is_sanitize'
            }));
        }
    }
    function createAnalysisTabGroups(element, elementRegistry) {
        var analysisGroup = {
            id: 'static-analysis',
            label: 'Static Analysis',
            entries: []
        };
        analysisProps(analysisGroup, element);

        return [
            analysisGroup
        ];
    }


    this.getTabs = function (element) {
        var analysisTab = {
            id: 'analysis',
            label: 'Analysis',
            groups: createAnalysisTabGroups(element, elementRegistry)
        };
        var array = camunda.getTabs(element);
        array.unshift(analysisTab);
        // console.log('%c ES GIBT TABS!!', 'color: #ff0000; font-size: 42px;');
        // console.log(array);
        return array;
    }
}

SharedPropertiesProvider.$inject = ['eventBus', 'bpmnFactory', 'elementRegistry', 'elementTemplates', 'translate']

module.exports = {
    __init__: ['propertiesProvider'],
    propertiesProvider: ['type', SharedPropertiesProvider]
};

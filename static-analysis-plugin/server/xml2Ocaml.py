import networkx as nx

# XML-Namespaces #
##################
bpmn = lambda name: "{http://www.omg.org/spec/BPMN/20100524/MODEL}%s" % name
camunda = lambda name: "{http://camunda.org/schema/1.0/bpmn}%s" % name




# OCAML-Elemente #
##################
class Activity(object):
    def __init__(self, action):
        self._action = action
    def __repr__(self):
        return "(Activity (%s))" % self._action

class Allocate(object):
    def __init__(self, var):
        self._var = var
    def __repr__(self):
        return "Allocate %s" % self._var
    
class Source(object):
    def __init__(self, var):
        self._var = var
    def __repr__(self):
        return "Source %s" % self._var

class Write(object):
    def __init__(self, obj, field, var):
        self._obj = obj
        self._field = field
        self._var = var
    def __repr__(self):
        return "Write (%s, %s, %s)" % (self._obj, self._field, self._var)
    
class Read(object):
    def __init__(self, var, obj, field):
        self._var = var
        self._obj = obj
        self._field = field
    def __repr__(self):
        return "Read (%s, %s, %s)" % (self._var, self._obj, self._field)

class Copy(object):
    def __init__(self, var1, var2):
        self._var1 = var1
        self._var2 = var2
    def __repr__(self):
        return "Copy (%s, %s)" % (self._var1, self._var2)

class Sanitize(object):
    def __init__(self, outvar, invar):
        self._invar = invar
        self._outvar = outvar
    def __repr__(self):
        return "Sanitize (%s, %s)" % (self._outvar, self._invar)

class Cons(object):
    def __init__(self, first, second):
        self._first = first
        self._second = second
    def __repr__(self):
        return "Cons ((%s), (%s))" % (self._first, self._second)

class Sequence(object):
    def __init__(self, first, second):
        self._first = first
        self._second = second
    def __repr__(self):
        return "(Sequence (%s, %s))" % (self._first, self._second)
    
class Choice(object):
    def __init__(self, a, b):
        self._a = a
        self._b = b
    def __repr__(self):
        return "(Choice (%s, %s))" % (self._a, self._b)

class Parallel(object):
    def __init__(self, a, b):
        self._a = a
        self._b = b
    def __repr__(self):
        return "(Parallel (%s, %s))" % (self._a, self._b)

class Loop(object):
    def __init__(self, body):
        self._body = body
    def __repr__(self):
        return "(Loop (%s))" % self._body

class Nil(object):
    def __repr__(self):
        return "Nil"


# Hilfsfunktionen fuer loops #
##############################

class UntranslatedLoop(object):
    def __init__(self, begin, end, inner):
        self.begin=begin
        self.end=end
        self.inner=inner

def find_all_loops(G, all_edges):
    # Schleifen bestehen aus mindestens 3 Komponenten
    possible_loops = [c for c in nx.components.strongly_connected_components(G) if len(c) > 2]
    for l in possible_loops:
        begin = [t for f, t in all_edges if t in l and f not in l]
        end = [f for f, t in all_edges if f in l and t not in l]
        inner = l.difference(begin+end)
        yield UntranslatedLoop(begin, end, inner)
        if len(inner) > 2:
            yield from find_all_loops(G.subgraph(inner), all_edges)



# BMPN-Prozess-in-OCaml Uebersetzer #
#####################################

class Translator(object):
    def __init__(self, seq, elem_by_id):
        self.seq = seq
        self.by_id = elem_by_id
        self.variables = set()
        self.fields = set()
        self.variable_id = {}
        self.activities = []
        self._G = nx.DiGraph()
        for from_id, to_elems in seq.items():
            for to_id in [x.get('id') for x in to_elems]:
                self._G.add_edge(from_id, to_id)
        self._loops = {l.begin[0]: l for l in find_all_loops(self._G, self._G.edges())}
    

    def is_seq(self, elem):
        # genau ein nachfolger -> sequence
        return (elem.get('id') in self.seq 
                and len(self.seq[elem.get('id')]) == 1)
    
    def is_choice(self, elem):
        # genau zwei nachfolger -> choice
        return (elem.get('id') in self.seq 
                and len(self.seq[elem.get('id')]) == 2)
    
    def find_next_after_choice(self, elem):
        # stelle finden, wo die pfade wieder zusammen gehen
        # TODO
        a, b = self.seq[elem.get('id')]
        step = a
        gw_count = {"a" : 0, "b" : 0}
        def _next(sid):
            elem = self.seq.get(sid, [None])[0]
            if elem is not None and self.is_choice(elem):
                cgw = self.find_next_after_choice(elem)
                return self.seq[cgw.get('id')][0]
            else:
                return elem
        while a is not None and b is not None \
                and a is not b:
            sid = step.get('id')
            if step is a:
                a = _next(sid)
                if not b.tag.endswith("Gateway"):
                    step = b
                else:
                    step = a
            elif step is b:
                b = _next(sid)
                if not a.tag.endswith("Gateway"):
                    step = a
                else:
                    steb = b
        return a
    
    def record_variable_id(self, var, elemid):
        if var not in self.variable_id:
            self.variable_id[var] = []
        self.variable_id[var].append(elemid)

    
    def translate_script(self, elem):
        script = elem.find(bpmn("script")).text
        script = script.split("\n")[0]
        
        left, right = script.split("=")
        left = left.strip()
        right = right.strip().replace(";","")
        if "new" in right:
            self.variables.add(left)
            self.record_variable_id(left, elem.get("id"))
            if elem.get("is_sensitive") == "true":
                self.activities.append(Source(left))
                return Source(left)
            self.activities.append(Allocate(left))
            return Allocate(left)
        elif "." in left:
            obj, field = left.split(".")
            self.variables.add(obj)
            self.fields.add(field)
            self.variables.add(right)
            self.record_variable_id(obj, elem.get("id"))
            self.record_variable_id(field, elem.get("id"))
            self.record_variable_id(right, elem.get("id"))
            self.activities.append(Write(obj, field, right))
            return Write(obj, field, right)
        elif "." in right:
            obj, field = right.split(".")
            self.variables.add(obj)
            self.fields.add(field)
            self.variables.add(left)
            self.record_variable_id(left, elem.get("id"))
            self.record_variable_id(field, elem.get("id"))
            self.record_variable_id(obj, elem.get("id"))
            self.activities.append(Read(left, obj, field))
            return Read(left, obj, field)
        elif "(" in right and ")" in right:
            outvar = left
            invar = right.split("(")[1].split(")")[0]
            self.variables.add(outvar)
            self.variables.add(invar)
            self.record_variable_id(outvar, elem.get("id"))
            self.record_variable_id(invar, elem.get("id"))
            if elem.get('is_sanitize') == 'true':
                self.activities.append(Sanitize(outvar, invar))
                return Sanitize(outvar, invar)
            # sonstiger Funktionsaufruf als copy
            self.activities.append(Copy(outvar, invar))
            return Copy(outvar, invar)
        else:
            self.variables.add(left)
            self.variables.add(right)
            self.record_variable_id(left, elem.get("id"))
            self.record_variable_id(right, elem.get("id"))
            self.activities.append(Copy(left, right))
            return Copy(left, right)
    
    def translate_source(self, elem):
        message_id = elem.get("messageRef")
        name = self.by_id[message_id].get("name")
        #self.variables.add(name)
        self.record_variable_id(name, elem.get("id"))
        if elem.get('is_sensitive') == 'true':
            self.activities.append(Source(name))
            return Source(name)
        else:
            # TODO: nicht-sensitive Receives als Allocate?
            self.activities.append(Allocate(name))
            return Allocate(name)
             
        
    def translate(self, elem, leaf=False, stop_at=()):
        if elem.tag == bpmn('sendTask') or elem.tag == bpmn('endEvent'):
            for expr in elem.findall('.//'+camunda('expression')):
                vname = expr.text
                self.record_variable_id(vname, elem.get('id'))
                self.variables.add(vname)
            return Nil()
        elif elem.get('id') in self._loops:
            l = self._loops[elem.get('id')]
            first_elem = self.seq[elem.get('id')][0]
            after_loop = [x for x in self.seq[l.end[0]] if x.get('id') not in l.begin\
                          and x.get('id') not in l.inner][0]
            if first_elem.get('id') in l.end:
                first_elem = [x for x in self.seq[l.end[0]] if x.get('id') in l.inner][0]
            stop_at = list(stop_at) + [self.by_id[x] for x in l.begin + l.end]
            if after_loop in stop_at:
                return Loop(self.translate(first_elem, stop_at=stop_at))
            else: 
                return Sequence(Loop(self.translate(first_elem, stop_at = stop_at)),
                                self.translate(after_loop, stop_at=stop_at))
        elif not leaf and self.is_seq(elem) and \
                self.seq[elem.get('id')][0] not in stop_at:
            return Sequence(
                self.translate(elem, leaf=True, stop_at=stop_at),
                self.translate(self.seq[elem.get('id')][0], stop_at=stop_at)
            )
        elif elem.tag == bpmn('scriptTask'):
            return Activity(self.translate_script(elem))
        elif elem.tag == bpmn('receiveTask'):
            return Activity(self.translate_source(elem))
        elif self.is_choice(elem):
            if elem.tag.startswith(bpmn("parallel")):
                gw = Parallel
            else:
                gw = Choice
            close_gw = self.find_next_after_choice(elem)
            next_after = self.seq.get(close_gw.get("id"), [None])[0]
            a, b = self.seq[elem.get('id')]
            stop_at=set(stop_at).union([close_gw])
            if next_after is not None and next_after not in stop_at:
                return Sequence(
                    gw(
                        # TODO: sequence in choice?
                        self.translate(a, stop_at=stop_at),
                        self.translate(b, stop_at=stop_at)
                    ),
                    self.translate(next_after, stop_at=stop_at)
                )
            else:
                return gw(
                        # TODO: sequence in choice?
                        self.translate(a, stop_at=stop_at),
                        self.translate(b, stop_at=stop_at)
                )
        return "NOCH_NICHT_UEBERSETZT: %s" % elem.tag


    def translate_list_activities(self, elem):
        self.translate(elem)
        ocaml_notation = Cons(self.activities[0], Nil())
        for activity in self.activities[1:]:
            ocaml_notation = Cons(activity, ocaml_notation)
        print(ocaml_notation)
        return ocaml_notation





# Hilfsfunktionen fuer XML-Verarbeitung #
#########################################

def get_sequence_arrows(process):
    out = {}
    for sf in process.findall('.//' + bpmn('sequenceFlow')):
        target_elem = process.findall(".//*[@id='%s']" % sf.get('targetRef'))
        l = out.get(sf.get('sourceRef'), [])
        l.extend(target_elem)
        out[sf.get('sourceRef')] = l
    return out

def get_element_by_id_map(process):
    out = {}
    for elem in process.findall('.//*[@id]'):
        out[elem.get('id')] = elem
    return out


# Hilfsfunktionen fuer OCaml-Code-Generierung #
###############################################

def build_variable_list(variables):
    ocaml_variables = ""
    last = None
    for var in variables:
        if ocaml_variables == "":
            ocaml_variables += "let %s = O\n" % var
        else:
            ocaml_variables += "let %s = S %s\n" % (var, last)
        last = var
    return ocaml_variables

def build_results(var):
    var = list(var)
    result = """let stub = \n analysis (list_to_process process)\n\n"""
    for v in var:
        result += """let result_%s = (match stub %s with
  | NotTainted -> "Not tainted"
  | MaybeTainted -> "Maybe tainted")\n""" %(v,v)
    result += "\nlet () = Printf.printf \"{\\\"%s\\\":\\\"%%s\\\"" %var[0]

    for v in (var[1:] if len(var) > 1 else []):
        result += ", \\\"%s\\\": \\\"%%s\\\"" % v
    
    result += "}\\n\" "
    for v in var:
        result += "result_%s " % v
    return result

def translate_bpmn(tree):
    root = tree.getroot()
    process = root.findall(bpmn('process'))[0]
    start = process.findall(bpmn('startEvent'))[0]

    seq = get_sequence_arrows(process)
    elem_by_id = get_element_by_id_map(root)

    # betrachtet nicht mehrere ausgehene Pfeile vom Start
    start_arrow = seq[start.get('id')][0]

    t = Translator(seq, elem_by_id)
    ocaml_process = "let process = " + str(t.translate_list_activities(elem=start_arrow)) + "\n"


    all_vars = build_variable_list(t.variables)
    all_fields = build_variable_list(t.fields)

    sink_vars = [v for v in t.variables 
        if any(elem_by_id[vid].tag == bpmn('sendTask') for vid in t.variable_id[v])]
    
    #import pdb; pdb.set_trace()
    print("Analysing for sinks: {}".format(sink_vars))
    results = build_results(sink_vars) if sink_vars else "let () = Printf.printf \"{}\""

    ocaml_program = all_vars + all_fields + ocaml_process + results

    # variable_id auf relevante vars und Senken einschraenken:
    t.variable_id = {
        v: [
            i for i in t.variable_id[v] if elem_by_id[i].tag.endswith("sendTask")
        ]
        for v in t.variable_id if v in sink_vars
    }

    return ocaml_program, t
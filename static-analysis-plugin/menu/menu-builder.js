'use strict';

var electron = require('electron'),
    Menu = electron.Menu,
    MenuItem = electron.MenuItem,
app = electron.app;

function MenuBuilder(opts) {
    this.opts = merge({
      appName: app.name,
      state: {
        dmn: false,
        activeEditor: null,
        cmmn: false,
        bpmn: true,
        undo: false,
        redo: false,
        editable: false,
        copy: false,
        paste: false,
        searchable: false,
        zoom: false,
        save: false,
        closable: false,
        elementsSelected: false,
        dmnRuleEditing: false,
        dmnClauseEditingfalse: false,
        exportAs: false,
        development: app.developmentMode,
        devtools: false
      },
      plugins: []
    }, opts);
  
    if (this.opts.template) {
      this.menu = Menu.buildFromTemplate(this.opts.template);
    } else {
      this.menu = new Menu();
    }
  }
  
  module.exports = MenuBuilder;
  
  MenuBuilder.prototype.appendAppMenu = function() {
    return this;
  };

  MenuBuilder.prototype.appendFileMenu = function(submenu) {
    this.menu.append(new MenuItem({
      label: 'File',
      submenu: submenu
    }));
  
    return this;
  };
  
  MenuBuilder.prototype.appendNewFile = function() {
    this.menu.append(new MenuItem({
      label: 'New File',
      submenu: Menu.buildFromTemplate([{
        label: 'BPMN Diagram',
        accelerator: 'CommandOrControl+T',
        click: function() {
          app.emit('menu:action', 'create-bpmn-diagram');
        }
      }, {
        label: 'DMN Table',
        click: function() {
          app.emit('menu:action', 'create-dmn-table');
        }
      }, {
        label: 'DMN Diagram',
        click: function() {
          app.emit('menu:action', 'create-dmn-diagram');
        }
      }, {
        label: 'CMMN Diagram',
        click: function() {
          app.emit('menu:action', 'create-cmmn-diagram');
        }
      }])
    }));
  
    return this;
  };
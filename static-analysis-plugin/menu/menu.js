'use strict';
var electron = require('electron');
var Menu = electron.Menu;
var MenuItem = electron.MenuItem;

var win = null;

const { ipcMain } = require('electron');

var _rq = [];

ipcMain.on('senddiagram', (event, arg) => {
  var request = require('request');

  // console.log("got arg: " + arg);
  _rq.forEach(r => r.abort());
  _rq.splice(0, _rq.length);

  var r = request.post('http://localhost:1511/hello', { json: true, body: arg }, (err, res, body) => {
    if (err) {
      return console.error('upload failed:', err);
    }
    var results = res.body
    win.webContents.send('analysisResults', results);
  });

  _rq.push(r);

});


module.exports = function (electronApp, menuState) {
  win = electronApp.mainWindow;
  return [{
    label: 'Start Analysis',
    accelerator: 'CommandOrControl+<',
    enabled: function () {
      return true;

    },
    action: function () {
      //Anfrage vom Menü an Canvas
      win.webContents.send('getdiagram');
    }
  }
  ];
}


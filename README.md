# Camunda Static Analysis Plugin

## Description
A static analysis plugin for the Camunda Modeler

## Installation
1. Install Docker  
See [Get Docker](https://docs.docker.com/install/linux/docker-ee/ubuntu/)
2. Run `start.sh`

## Build Process
Running `start.sh` will do the following:  
1. Download the Camunda Modeler (if not done yet)
2. Compile the plugin for the Camunda Modeler using [Node](https://hub.docker.com/_/node/)
3. Set a symbolic link from the `plugins` folder of the Camunda Modeler  
to the folder `static-analysis-plugin`, which contains the source code for the plugin
4. Build the docker image `analysis-plugin` following the instructions in `DockerAnalysisServer/Dockerfile`
5. Start the server 
6. Start the Camunda Modeler


